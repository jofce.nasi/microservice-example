package com.test.tokenstoreTest.model;

public enum Role {
    WEB_USER, ADMIN_USER
}
