package com.test.tokenstoreTest;

import com.test.tokenstoreTest.model.Role;
import com.test.tokenstoreTest.model.User;
import com.test.tokenstoreTest.persistence.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.stereotype.Component;

@SpringBootApplication
@EnableDiscoveryClient
public class TokenstoreTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(TokenstoreTestApplication.class, args);
	}

	@Component
	public class DataLoader implements ApplicationRunner {

		private UserRepository userRepository;

		@Autowired
		public DataLoader(UserRepository userRepository) {
			this.userRepository = userRepository;
		}

		public void run(ApplicationArguments args) {
			User user = new User();
			user.username = "admin";
			user.password = "password";
			user.role = Role.ADMIN_USER;
			userRepository.save(user);
		}
	}
}
