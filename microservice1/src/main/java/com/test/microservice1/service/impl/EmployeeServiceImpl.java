package com.test.microservice1.service.impl;

import com.test.microservice1.model.Employee;
import com.test.microservice1.persistance.EmployeeRepository;
import com.test.microservice1.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public Employee findById(Integer id) {
        return employeeRepository.findById(id).orElse(new Employee());
    }

    @Override
    public Employee save(Employee employee) {
        return employeeRepository.save(employee);
    }
}
