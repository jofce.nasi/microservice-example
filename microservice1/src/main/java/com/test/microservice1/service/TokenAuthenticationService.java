package com.test.microservice1.service;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.stereotype.Service;

@Service
public class TokenAuthenticationService {

    private final String SECRET = "ThisIsASecret";

    public String getRole(String token) {

        Claims body = Jwts.parser()
                .setSigningKey(SECRET)
                .parseClaimsJws(token)
                .getBody();

        return body.get("role").toString();
    }
}
