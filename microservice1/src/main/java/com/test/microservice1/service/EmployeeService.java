package com.test.microservice1.service;


import com.test.microservice1.model.Employee;

public interface EmployeeService{

    Employee findById(Integer id);

    Employee save(Employee employee);
}
