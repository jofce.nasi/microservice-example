package com.test.microservice1.web;

import com.test.microservice1.model.Employee;
import com.test.microservice1.service.EmployeeService;
import com.test.microservice1.service.TokenAuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class Controller {

    @Autowired
    private TokenAuthenticationService tokenAuthenticationService;

    @Autowired
    private EmployeeService employeeService;

    @GetMapping("/index")
    public String index(){
       return "Hello from microservice 1";
    }

    @GetMapping("/employee/{id}")
    public Employee getEmployee(@PathVariable Integer id){
        return employeeService.findById(id);
    }

    @PostMapping("/employee")
    public Employee saveEmployee(@RequestBody Employee employee){
        return employeeService.save(employee);
    }
}
