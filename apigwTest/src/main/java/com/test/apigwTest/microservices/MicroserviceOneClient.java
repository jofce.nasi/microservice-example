package com.test.apigwTest.microservices;

import com.test.apigwTest.model.EmployeeDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(value = "microservice1")
public interface MicroserviceOneClient {

    @GetMapping("/index")
    String index();

    @GetMapping("/employee/{id}")
    EmployeeDTO getEmployee(@PathVariable Integer id);

    @PostMapping("/employee")
    public EmployeeDTO saveEmployee(@RequestBody EmployeeDTO employee);
}
