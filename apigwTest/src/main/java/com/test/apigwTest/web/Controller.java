package com.test.apigwTest.web;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.test.apigwTest.microservices.MicroserviceOneClient;
import com.test.apigwTest.model.EmployeeDTO;
import com.test.apigwTest.service.TokenAuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class Controller {

    @Autowired
    private MicroserviceOneClient microserviceOneClient;

    @Autowired
    private TokenAuthenticationService tokenAuthenticationService;

    @GetMapping("/index")
    public String index(){
        return "This is index";
    }

    @HystrixCommand(fallbackMethod = "fallback")
    @GetMapping("/client-index")
    public String clientIndex(@RequestHeader("Authorization") String authorization){
        if(tokenAuthenticationService.getRole(authorization.substring(7)).equals("ROLE_ADMIN")){
            return microserviceOneClient.index();
        }
        else{
            return "Unauthorized";
        }
    }

    @GetMapping("/employee/{id}")
    public EmployeeDTO getEmployee(@PathVariable Integer id){
        return microserviceOneClient.getEmployee(id);
    }

    @PostMapping("/employee")
    public EmployeeDTO saveEmployee(@RequestBody EmployeeDTO employee){
        return microserviceOneClient.saveEmployee(employee);
    }

    @GetMapping("/fallback")
    public String fallback(){
        return "Fallback";
    }
}
